﻿using System;
using System.Threading;

namespace M09UF2E1_XaviGuillamón
{
    class Program
    {
        static void Main(string[] args)
        {
            string phrase1 = "Hello, how are you?";
            string phrase2 = "I'm fine, I want some drinks";
            string phrase3 = "Let's put some beers in the fridge and drink! \n";

            Thread thread1 = new Thread(() => PrintPhrase(phrase1));
            Thread thread2 = new Thread(() => PrintPhrase(phrase2));
            Thread thread3 = new Thread(() => PrintPhrase(phrase3));

            thread1.Start();
            thread1.Join();

            thread2.Start();
            thread2.Join();

            thread3.Start();
            thread3.Join();
        



        
            Fridge fridge = new Fridge();

            Thread quevedoThread = new Thread(() => fridge.FillFridge("Quevedo"));
            Thread badBunnyThread = new Thread(() => fridge.DrinkBeer("Conejo Malo"));
            Thread lilNasXThread = new Thread(() => fridge.DrinkBeer("Pequeña Nariz"));
            Thread manuelTurizoThread = new Thread(() => fridge.DrinkBeer("Turizo"));


            quevedoThread.Start();
            quevedoThread.Join();
            badBunnyThread.Start();
            lilNasXThread.Start();
            manuelTurizoThread.Start();
        
        }
        static void PrintPhrase(string phrase)
        {
            string[] words = phrase.Split(' ');
            foreach (string word in words)
            {
                Console.Write(word + " ");
                Thread.Sleep(500);
            }
            Console.WriteLine();
        }
        class Fridge
        {
            private int maximumBeers;

            public Fridge()
            {
                maximumBeers = 6;
            }

            public void FillFridge(string name)
            {
                lock (this)
                {
                    int beersAdded = new Random().Next(1, 7);
                    if (maximumBeers + beersAdded > 9)
                    {
                        beersAdded = 9 - maximumBeers;
                    }
                    maximumBeers += beersAdded;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("{0} added {1} beers on the fridge", name, beersAdded);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.WriteLine("\n{0} beers remaining.\n", maximumBeers);
                    Console.ResetColor();
                }
            }
            public void DrinkBeer(string name)
            {
                lock (this)
                {
                    int beersDrink = new Random().Next(1, 7);
                    if (maximumBeers - beersDrink < 0)
                    {
                        beersDrink = maximumBeers;
                    }
                    maximumBeers -= beersDrink;
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("{0} drink {1} beers", name, beersDrink);
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.WriteLine("\n{0} beers remaining.\n", maximumBeers);
                    Console.ResetColor();
                }
            }
        }
    }
}
